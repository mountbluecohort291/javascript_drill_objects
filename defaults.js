export function defaults(obj, defaultProps) {
  if (
    typeof obj == "object" &&
    obj != null &&
    Object.keys(obj).length != 0
  ) {
    for (let key in defaultProps) {
      if (obj[key] == undefined) {
        //check if the key is undefined in teh object
        obj[key] = defaultProps[key];
      }
    }                                                                
  }
  return obj;
}
