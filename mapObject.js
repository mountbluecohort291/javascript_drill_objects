export function mapObject(obj, cb) {
  let result = {};
  if (typeof obj == "object" && obj != null && Object.keys(obj).length != 0) {
    for (let [key, value] of Object.entries(obj)) {
      result[key] = cb(key, value, obj); // stores the returned value as new result object property
    }
  }
  return result;
}
