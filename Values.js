export function myValues(testObject) {
  if (
    typeof testObject == "object" &&
    testObject != null &&
    Object.keys(testObject).length != 0
  ) {
    let values = [];
    for (let key in testObject) {
      if (testObject.hasOwnProperty(key)) {
        //check if key was explicitely defined within the object and not prototype.
        values.push(testObject[key]); //returns values as arrays
      }
    }
    return values;
  } else {
    return [];
  }
}
