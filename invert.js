export function invert(obj) {
  let inverted = {};
  if (typeof obj == "object" && obj != null && Object.keys(obj).length != 0) {
    for (let [key, value] of Object.entries(obj)) {
      inverted[value] = key; //Inverts The Key Value Pair Of The Object
    }
  }
  return inverted;
}
