export function pairs(obj) {
  let resultPairs = [];
  if (typeof obj == "object" && obj != null && Object.keys(obj).length != 0) {
    for (const [key, value] of Object.entries(obj)) {
      resultPairs.push([key, value]);
    }
  }
  return resultPairs; //returns the testPairs
}
