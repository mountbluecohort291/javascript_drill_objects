## Javascript_Drills_Object
### Overview

This repository contains JavaScript drills focused on object manipulation and operations. The drills are designed to enhance your understanding and skills in handling objects in JavaScript.
Getting Started
Prerequisites

- Ensure you have Node.js and npm (Node Package Manager) installed on your machine.
Installation

- To install the necessary dependencies, run the following command:

bash
``
npm install
``
Structure

The repository is structured as follows:

    root: This folder contains the six problem files and a data.js file.
    test: This folder contains the six test files to test the corresponding problem files.

Running Tests

To run the test files, use the following command:

bash

`node ./test/<testfilename.js>`

Replace <testfilename.js> with the name of the test file you want to run. For example, to run the test file for the first problem, you would use:
Example: 
`` node ./test/test1.js``

## Commit Structure

- Each problem file and its corresponding test file have been committed separately to maintain a clear and organized version history.
## Author

Vishal-Anthony &copy;2024

This project is licensed under the MIT License. 