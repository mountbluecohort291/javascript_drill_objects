export function myKeys(testObject) {
  if (
    typeof testObject == "object" &&
    testObject != null &&
    Object.keys(testObject).length != 0
  ) {
    let keys = [];
    for (let key in testObject) {
      if (testObject.hasOwnProperty(key)) {
        //check if key was explicitely defined within the object and not prototype
        keys.push(key);
      }
    }
    return keys;
  } else {
    return []; //retuern empty array if edge cased
  }
}
