import { mapObject } from "../mapObject.js";
const testObject = { one: 1, two: 2, three: 3 };
const transformedObjects = mapObject(testObject, function (key, value, obj) {
  return value * 2;
});
console.log(transformedObjects); //{ one: 2, two: 4, three: 6 }
